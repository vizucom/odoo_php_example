# PHP -> Odoo v8 connection examples using XMLRPC #

### Included examples: ###
* Fetching all employees
* Fetching all customer companies
* Fetching all leads/opportunities

### Requirements: ###
* XML-RPC package (apt-get install php5-xmlrpc)
* openerp-php-connector (https://github.com/tejastank/openerp-php-connector)