<?php
/*
*
* PHP -> Odoo v8 connection examples using XMLRPC
* Timo Talvitie / Vizucom
* timo.talvitie@vizucom.com
*
* -Fetching all employees
* -Fetching all customer companies
* -Fetching all leads/opportunities
*
* -Requirements:
*  * XML-RPC package (apt-get install php5-xmlrpc)
*  * openerp-php-connector (https://github.com/tejastank/openerp-php-connector)
*
*/


// Import the openerp-php-connector
require_once('openerp-php-connector/openerp.class.php');


// Log in with credentials: username, password, database name, and URL
// * Remember to add /xmlrpc/ suffix to URL, as well as port if needed
// * E.g. http://myhost.com:8078/xmlrpc/

$instance = new OpenERP();
$instance->login("USERNAME", "PASSWORD", "DATABASENAME", "http://HOST:PORT/xmlrpc/");

// Define the model names we want to pull data from
// List of models is available in Odoo at Settings -> Technical -> Database Structure -> Models
$employee_model	= 'hr.employee';
$partner_model	= 'res.partner';
$lead_model	= 'crm.lead';


// EXAMPLE 1 - Get names and emails of all employees
// ========================================================


// Step 1. Define which fields we want to read
// Also the list of fields for each model is available in Odoo at Settings -> Technical -> Database Structure -> Models
$employee_fields = array('id','name','work_email');


// Step 2. Define which employees' data we want to read
// This is done with a search query that returns a list of employees' database ids that match the search criteria
// More info on the search query at https://www.odoo.com/forum/help-1/question/xml-rpc-search-arguments-10738

// Examples of search criteria
// a) Empty - use when you want to fetch all records
$search_criteria_a = array();

// b) Single search criterion - Three-parameter syntax: 1) Odoo field, 2) operator, 3) Value to match 
$search_criteria_b = array(
	array('name','like','John%'),
);

// c) Multiple criteria: Same three-parameter syntax. These are combined into an AND-based SQL statement 
$search_criteria_c = array(
	array('name','like','John%'),
	array('work_location','=','London'),
);

// Since we want all employees, use a)
// The result is a list of matching employee database ids in Odoo
$employee_ids = $instance->search($search_criteria_a, $employee_model);
print "\nMatching employee ids:\n";
print_r($employee_ids);


// Step 3. Once we have all the parameters ready, perform the actual read operation
$employees_result = $instance->read($employee_ids, $employee_fields, $employee_model);

// The results are provided as an array from which they can be accessed
print "\nSearch results for employees\n";
print_r($employees_result);




// EXAMPLE 2 - Get names of all customer companies
// ===============================================


// Step 1. Define which fields we want to read
$customer_fields = array('id','name');


// Step 2. Create search criteria and search
// In Odoo the res.partner model represents suppliers and customers, so filter
// the results to include only customers. Also, exclude any persons that have been
// marked as customers, and return only actual companies.

$search_criteria_customers = array(
	array('customer','=',True),
	array('is_company','=',True),
);

$customer_ids = $instance->search($search_criteria_customers, $partner_model);
print "\nMatching customer ids:\n";
print_r($customer_ids);


// Step 3. Perform the read operation
$customers_result = $instance->read($customer_ids, $customer_fields, $partner_model);
print "\nSearch results for customers\n";
print_r($customers_result);




// EXAMPLE 3 - Get all leads/opportunities
// =======================================


// Step 1. Define which fields we want to read (for leads there are lots to choose from
// so you probably want to reconfigure these to fit your needs)
$lead_fields = array('id','name','description','user_id','partner_id','stage_id');


// Step 2. Create empty search criteria and search
$search_criteria_leads = array();

$lead_ids = $instance->search($search_criteria_lead, $lead_model);
print "\nMatching lead ids:\n";
print_r($lead_ids);

// Step 3. Perform the read operation
$leads_result = $instance->read($lead_ids, $lead_fields, $lead_model);
print "\nSearch results for leads\n";
print_r($leads_result);


?>
